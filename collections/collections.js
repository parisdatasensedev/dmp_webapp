Topics = new Mongo.Collection('topics')
Keywords = new Mongo.Collection('keywords')
TopicKeywords = new Mongo.Collection('topic_keywords')
Influencers = new Mongo.Collection('influencers')
InfluencerTopics = new Mongo.Collection('influencer_topics')

TwitterAccounts = new Mongo.Collection('twitter_account')
TwitterPosts = new Mongo.Collection('twitter_posts')