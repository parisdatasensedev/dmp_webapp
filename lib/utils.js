export const _utils = {
    existsVarTree: (variable, path) => {
        const pathEntries = path.split('.')
        let ifExists = true
        let start = variable
        pathEntries.forEach(function (pe) {
            if (ifExists) {
                if(_.isUndefined(start[pe])) {
                    ifExists = false
                } else {
                    start = start[pe]
                }
            }
        })
        return ifExists
    }
}