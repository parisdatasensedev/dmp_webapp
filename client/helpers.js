
/**
 * Default Helpers
 */

Template.registerHelper('compare', function (v1, v2) {
    if (typeof v1 === 'object' && typeof v2 === 'object') {
        return _.isEqual(v1, v2); // do a object comparison
    } else {
        return v1 === v2;
    }
});

Template.registerHelper('formatDate', function (d) {
    return d.getDate() + '/' + (d.getMonth() +  1) + '/' + d.getFullYear()
});

Template.registerHelper('dump', function (d) {
    return JSON.stringify(d)
});

Template.registerHelper('type', function (d, type) {
    if (type == "list") {
        return _.isArray(d)
    }
    if (type == "string") {
        return _.isString(d)
    }
    if (type == "object") {
        return _.isObject(d)
    }

    return false
});

Template.registerHelper('arrayIndex', (d, i) => {
    return d[i]
})

