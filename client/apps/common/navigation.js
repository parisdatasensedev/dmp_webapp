Template.Navigation.rendered = function(){

    // Initialize metisMenu
    $('#side-menu').metisMenu();

};

Template.Navigation.helpers({

    userName: () => {
        let user = Meteor.user()
        if(user && user.emails) {
            if(user.emails.length > 0)
            return user.emails[0].address.toUpperCase()
        }
    }

})