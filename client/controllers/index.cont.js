FlowRouter.route('/', {
    name: "landing",
    action() {
        FlowRouter.go('home')
    }
})

FlowRouter.route('/home', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', {
            body: 'Overview'
        })
    }
})


